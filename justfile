default:
	just --list --justfile {{justfile()}}

_dirs:
	for dir in $(fd -t d . -E public); do mkdir -p "public/$dir"; done
	for dir in $(fd -t f . -e md -E index.md | sed 's/.md//'); do mkdir -p "public/$dir"; done
	rm -r public/templates

_css: _dirs
	sassc css/main.scss public/css/main.css

_convert file:
	#!/usr/bin/env bash
	set -euxo pipefail
	outfile={{ if file_name(file) != 'index.md' { join("public", replace(file, ".md", ""), "index.html") } else { join("public", replace(file, ".md", ".html")) } }}
	pandoc --template=templates/base {{file}} -o $outfile

_clean file:
	if [ -e {{file}} ]; then rm -r {{file}}; fi

_serve:
	just build
	python -m http.server -d public -b 127.0.0.1

build: _css _dirs
	for file in $(fd -e md .); do just _convert $file; done
	sh toc.sh | pandoc --template=templates/base -o public/blog/index.html --metadata title="Blog | CS"
	sh rss.sh > public/rss.xml

serve:
	fd . -E public | entr -r just _serve

clean:
	for dir in $(fd -t d . -E public); do just _clean "public/$dir"; done
	just _clean public/index.html
