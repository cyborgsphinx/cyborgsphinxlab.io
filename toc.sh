#! /bin/sh

maketoc() {
    for file in "$@"
    do
        date=$(grep 'date:' $file | sed 's/date: //')
        title=$(grep 'title:' $file | sed 's/title: //' | sed 's/| CS//')
        link=$(echo $file | sed 's/.md//')
        printf "* [%s] [%s](/%s)\n" "$date" "$title" "$link"
    done
}

# table of contents header
echo "## Contents"

# only non-draft posts
maketoc $(fd -e md . blog -x grep -L 'draft: *true') | sort -r
