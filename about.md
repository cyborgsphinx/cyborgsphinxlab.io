---
title: About
...

The first thing you should know about me is that I am. The second thing you should know about me is that you shouldn't.

I have a lot of thoughts and I need to put them somewhere so that I don't lose my mind. That's all this is really meant to be. You shouldn't take anything I write any more seriously than that demands.
