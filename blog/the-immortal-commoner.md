---
title: The Immortal Commoner
date: 2025-02-03
...

Imagine, if you will, a guy. Just some guy. Equipped for a dungeon crawl, with torches and stuff, but not really for combat. No armour, 1 hit point. This guy isn't going to last long in armed conflict. But what this guy does have is infinite time, to the point of being considered immortal.

Let's put this guy in a situation. A dungeon, specifically. Let's put this guy in a dungeon that has no other creatures. Maybe there never were any, maybe someone cleared them out already. Point is, no creatures but this guy. But the dungeon has a layout, and doors with locks, and secrets. And traps, maybe.

Having put this guy in that situation, I'd like to pose a question: Can the Immortal Commoner (this guy) reach every secret?

This is a device that's been bouncing around in my head for a while now. Can anybody reach every secret in this dungeon? Do some secrets require character choices made five hours ago to be slightly different? If the party has a combat that goes slightly wrong, are they locked out of half the fun of the space they're in?

I think by the framing of that last question, my preferred answer is pretty clear. I'd like for all the secrets to be accessible regardless of how many spells have been cast or whether or not there's a rogue in the party.

I think this preference comes from my history with video games, both positively and negatively. On the negative side, it's when you see things like doors locked unless you chose to be a rogue at character creation, or some other cost that requires you to be good at the game surprisingly early on. Design choices like these are for replayability. You're a knight this time, but oooooooo look, if you play as a rogue next time you can see what's behind this door! Well I want to see what's behind it now, so I can be sure that it actually is just early game gear.

I was struggling for a while to work out exactly why this principle appealed on the positive side as well. Not only do I *not* like when secrets are blocked like this, but I *do* like when they are all accessible in one playthrough. And I think, completionist tendencies aside, that the reason I like it shares a lot with the reason I'm actually okay with my players using spells to bypass challenges: I think sequence breaking is cool.

Let's go back to our guy. The fact that this guy can reach every secret in the dungeon means that there's at least one path through the dungeon that allows them to reach all the secrets. This path represents a sequence of events (go here, grab this key, go there, unlock the door, etc.) that will allow 100% completion of the dungeon. But if your players can reach something before it's unlocked in that sequence, that's a fun moment. Passwall to get through a locked door even though the key is in the next room? That's fun! Pick the lock because there's no key and you have to get inside to move forward? Less fun! Some might say not fun at all!

I've long been a little confused about people who don't want their players to use their spells to bypass their puzzles. "Oh, the wall is made of anti-magic rocks and your passwall fails" doesn't make your puzzle more fun to engage with, it just makes it necessary to engage with. I've had a lot more success getting player buy in by genuinely convincing them that interacting with things will be fun than trying to force them to engage with something I liked but they didn't.

Also, each passwall or fly or telekinesis is one more spell slot they can't use to cast fireball later. All future combats just got a little harder, and all the players did was avoid a lock.
