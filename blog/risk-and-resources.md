---
title: Risk Management Is Not Resource Management
date: 2022-12-10
...

Genres are weird, aren't they? Categories are supposed to make things easier, but sometimes we reach a point where the anticipated experience differs from the actual experience, because the chosen genre demarcations aren't flexible enough to allow for the necessary nuance.

This isn't to say that genres are bad, or that they don't pose any use. Black metal and power metal may sound very different to listen to, but they've got more in common than either does with, say, country music. It's that commonality that is being referenced when we talk about heavy metal music as a whole. It's just that recommending black metal based on an enjoyment of power metal may not be met with success.

This mismatch in terms of recommendation has proven inconvenient for Dungeons and Dragons recently. The table-top roleplaying game space is incredibly diverse, with all sorts of different ways to approach both narrative and gameplay. But people centre around playing D&D, especially people new to the hobby. This comes in no small part because so many other people are playing it, and it's become synonymous with the space to outsiders.

This has led to a lot of discussion around whether the game is a good ambassador for the hobby, and even questioning what, if anything, [the game is even about](https://www.youtube.com/watch?v=BQpnjYS6mnk).

There are arguments that it used to be about resource management, and that there are some holdovers from that time, but it isn't really about that anymore. I think that's a reasonable argument, especially since I haven't seen much evidence that people are using the modern rules to run resource management-heavy games.

But D&D does feel like it's about something, to me at least. I wouldn't call it a brainless game by any stretch of the imagination, and I think it does encourage a certain style of play, even if that style might not fit into the moulds we're told to expect.

So I'd like to look at two video game series and see what I can learn about what they require of the player in terms of management. Specifically, I'm going to use Fire Emblem to represent resource management, and XCOM to represent what I'm calling "risk management". After that, I'm going to take what I learned and see if I can apply it to the storied history of D&D, and how it's evolved over the years.[^1]

[^1]: I wasn't actually alive for many of those years, and I didn't really start playing until 5e was fairly well established. Still, I'm hoping that I can make use of other people's experiences to help guide my analysis.

Fire Emblem: Resource Management
--------------------------------

Fire Emblem is a turn-based tactics game series without a consistent setting, but typically taking place in a medieval-adjacent fantasy world with magic and dragons. You could reasonably set a D&D game in most Fire Emblem settings. While politics and drama and the [numerous characters](https://www.youtube.com/watch?v=EMVjPDqrkyg) are a primary focus of the narrative of these games, the gameplay itself really happens in battle[^2], which is where the resource management elements come into play.

[^2]: To try to make things easier to follow, I'm going to try to maintain a distinction between battles, the larger unit of gameplay where you move units around a map, and fights, which are a single instance of violence in which attacks are made, with or without the chance for a counterattack. Battles involve taking turns, during which an individual unit may fight another.

Being a turn-based tactics game, there's a map with a grid, your units, enemy units, sometimes NPC allied units, and you take turns moving around the map trying to achieve some main objective, whether that's kill all the enemies, kill some of the enemies, survive some number of turns, or something else entirely. To fight an opponent, you equip your unit with a weapon, move close enough to an enemy unit to use that weapon, and pray that they don't miss an attack with a 90% chance to hit. This brings us to two of the main resources being managed: weapon durability, and your character's health.

Each weapon in the game (with some exceptions in some games) has a limited number of uses, after which it is broken and (usually) can't be used again. Weapons consume uses on offense and defence, so your unit can wind up breaking a weapon in the middle of an enemy turn and have no equipment with which to defend themselves. Each character also has a limited number of inventory slots, which must be shared between weapons, healing items, keys, and any other items they wish to carry. You can't just load up all your Iron Swords on your dedicated Sword Guy™ if you want him to be able to heal himself.

Some weapons also have extra traits, like bonuses when used against (for example) armoured units. These weapons often take up an inventory slot only to be used against two or three enemy units on a mission, but they can make dealing with those units far easier than they would be otherwise. They are often also rarer or more expensive than the regular weapons, increasing their value even more.

There's also a rock-paper-scissors-like mechanic called the Weapon Triangle, where swords are good against axes which are good against spears which are good against swords. Melee weapons outside of this triangle are hard to come by, so most units are stuck trying to make the most of what they have. Even being able to use more than one weapon type is often limited to certain classes, so your dedicated Sword Guy™ may be stuck with a disadvantage against half of the enemy units in some cases.

On top of that, you have to actively manage the health of your units. Attacking an enemy usually invokes a counterattack, unless that enemy could not reach your unit with its current weapon. So unless your unit can kill them outright or dodge the attack, they aren't coming out unscathed. Since you typically have fewer units on any map than there are enemy units, this means that careless health management can lead to a lost unit, reducing the overall damage output of your army.

Not only does it reduce the damage output during that fight, but Fire Emblem is a game series that embraces permanent character death (colloquially called permadeath), so your dead units stay dead.[^3] This means that not only is unit health a resource being managed, but units themselves are as well. It's entirely possible to wind up in a situation late in the game where you have fewer units than you have capacity to deploy, and you will simply have to fight on with that handicap.

[^3]: Some games have limited ways to bring back dead units, and more recent ones (since Fire Emblem: Awakening) have a "casual" mode that ignores permadeath. The former doesn't contradict my point about resource management, and the latter is explicitly for people who want to ignore managing that resource. I will be ignoring casual mode when talking about permadeath.

There's also money, a resource that isn't used directly in fights but still has a significant influence on them. When weapons break, healing items get used, or any number of other items are needed, they can be purchased from an item shop. Even promoting a unit to a stronger class requires purchasing an item. In many of the old games[^4], the shops appear on the battle map, forcing you to spend the full turn of one of your units moving to and engaging with the shop. This requires choosing which one to shop with, as items are put into that character's inventory and may need to be ferried to the intended character on subsequent turns, as well as removing a possible attack from your turn. More things to think about, more resources to manage.

[^4]: This feature has largely been removed from the series since Fire Emblem: Awakening).

The resources being managed throughout a game of Fire Emblem are often as much a deciding factor for how to manoeuver your units and attack the enemy as the chances to hit. I have found on numerous occasions that I will take a reduced chance to hit if it means conserving uses of a weapon that I can use later, or if it means that the enemy unit has an even lower chance to successfully retaliate. This is what I'm talking about when I call Fire Emblem a turn-based tactics game about resource management.

But why the distinction? How different can a game that does broadly the same thing really feel? Well...

XCOM: Risk Management
---------------------

XCOM is a turn-based tactics game series about defending Earth from alien invasion. It has taken on a few forms over the years, but since XCOM: Enemy Unknown[^5] seems to be relatively stable in terms of the style of play it employs. The recent games cycle between managing your base and engaging in battles. Since the base management part of the game isn't really turn-based or tactics, I'm going to focus on the battles, which I would argue are less about managing resources than they are about managing risk.

[^5]: I've only played the Firaxis games, so I'm only going to talk about the Firaxis games. My only exposure to earlier XCOM games is stories of early game strategies of arming units with only grenades and running them into the aliens. I'm not sure what kind of management this is, but it doesn't really fit with my goals in this post. The reason I can talk about earlier Fire Emblem games with any confidence is that I've come across people talking about them in much more depth than just laughing at early game strategy, and they don't seem completely at odds with my own experiences with the newer games. That isn't the case with the older XCOM games.

Much like Fire Emblem, the battles in XCOM games take place on a grid, with the player controlling a number of units and having to fight against enemy units to finish the battle. However, fights do not typically allow for a counterattack, and there's some variance in damage output even at higher levels. This means that you can make an attack thinking it will finish the enemy off, but they survive on one hitpoint and kill your unit on their turn. Fights also take place at range, allowing you to attempt to shoot from behind cover for better defense on the enemy turn.

Weapon uses are also different. Guns in XCOM have limited magazine sizes but with unlimited total ammunition, and each unit carries a fixed loadout depending on their class. It is also possible to forgo shooting in order to move farther, allowing for either hasty retreats or aggressive pushes. Some weapons, like the sniper rifle, require that the unit does not move before using them, and since attacking means exhausting that unit's turn, using a sniper rifle requires that the unit stay where they are.

Healing during a battle is limited to once or twice per healing unit, and must be done by one unit to another. This makes health act more monotone, ticking down as the battle wears on.

The fog of war, an element from select maps in Fire Emblem games, is prevalent throughout XCOM games. Units have limited vision, and you can only see enemies that one of your units can see. This can lead to surprise attacks as the enemy units flank your own, or stumbling into a group unprepared and having to scramble to recover. There is item to expand the visible range, either, unlike Fire Emblem which uses yet another limited-use item that has to be in a unit's inventory.

Some actions from the enemy units can result in your units becoming panicked, forcing them to stay still and use their action to do something random, like hunker down (increasing the cover defense bonus) or shoot a random target, sometimes one of their own allies.

Permadeath is present, but in a different capacity. Where Fire Emblem has a limited pool of possible units to recruit, and missing out on them also misses out on their story, XCOM games[^6] have randomized characters and allow the purchase of new units between any battle, so long as you have enough money. Squad sizes typically range from 4 to 6, however, instead of the 10 or more that's common in Fire Emblem. This means that any death can significantly reduce your effective damage output for that battle, and it's harder to fit lower level units into a squad to get them levelled up.

[^6]: Except XCOM: Chimera Squad, in which all characters are pre-defined (much like Fire Emblem) and any confirmed death results in a game over.

XCOM 2 introduced hard time limits to battles, requiring that some objective be completed before a given turn. While this is present in a few key levels in Fire Emblem games, XCOM 2 is full of them. The turn limits push you to advance even when it's not completely safe to do so, driving the battle towards some conclusion - whether it's success or failure.

These result in a style of play that I'm calling risk management, where the goal is to reduce the risk to your units while trying to still output some damage. The lead designer has even said that [he thinks risks are what make the game](https://www.rockpapershotgun.com/making-of-xcom-2):

> [...] I think that the game is at its best when the player is taking risks.

The choices being made at a design level feed into forcing the player to take risks, which is not true to the same degree in Fire Emblem. A battle in Fire Emblem is about using your resources to the best of your ability, taking a few risks along the way which are often guided by the resources available to you.[^unless] A battle in XCOM is about trying to survive overwhelming odds against an enemy that is often better prepared than you, and trying to limit the damage of doing so.

[^unless]: Unless you're trying to play faster, whether for speedrunning, low turn count challenge runs, or a belief that the turtle approach is incredibly boring. All of those are valid, I just don't think that Fire Emblem is necessarily designed with those choices in mind.

Thinking Atop The Table
-----------------------

So what? Dungeons & Dragons is a completely different game from Fire Emblem and XCOM. It's an analogue, collaborative, role play-focused game. It also has combat that takes place on a grid, with the players on one side fighting against units controlled by the Dungeon Master in order to finish... Wait. That sounds familiar.

Okay, so maybe there's some similarities. And the [original game was derived from a wargame](https://en.wikipedia.org/wiki/Miniature_wargaming#Role-playing_games), which also seem to resemble the format of the modern Fire Emblem and XCOM games. And I've been hearing a lot from people who have played the original and who play the more recent clones that the original game was more about resource management than its current iteration, which brings to mind the above comparison.

I'd also like to point to some table top RPGs that aren't D&D (or D&D-likes[^like]) that I think fairly neatly highlight what this might look like in the analog space.

[^like]: I mean you, Pathfinder and 13th Age.

Blades in the Dark is a game about doing heists in a Dishonored-like city. It is so incredibly not about managing resources that you don't even need to declare what equipment you have with you until you have a use for it. Your character is simply understood to have packed the right tool for the job. At the same time, the game rewards taking risks, to the point where trying to do something while at a disadvantage is directly rewarded with experience points.

Fate is a game containing a back-and-forth resource, called Fate Points, which allows the player to activate powerful abilities or declare a fact about the scene they are in. To get Fate Points, the player must accept some negative effect of their character happening at some earlier point in the story. This stands in contrast with the dice mechanic, where each die adds to the range of possible values, but can be either positive or negative depending on the result. The bulk of any risk management is deciding when to do something that requires rolling dice.

The 2d20 system used by Modiphius in a number of games has its own collection of metacurrencies, each tied to rolling dice. The players can generate Momentum by generating more successes than necessary, and then spend it to get more dice in a future pool. There's also some metacurrencies that the DM can engage with, mostly by being given some tokens by the players and then cashing them in to force Bad Things™ to happen. In this way, the resources play into the risk management, so it's kind of both at once.

But back to D&D. I think it's fair to say that the 5<sup>th</sup> edition of D&D hasn't been very heavy on the resource management. Just about all resources that one might want to manage are completely unrelated to combat, and the rest are either consumable items or something that will come back after a narrative rest. The resources that ARE managed are often not actually managed.[^manage] The only other common resources are health and time, both of which could be used to argue in either direction.

[^manage]: I don't have my players track encumbrance or rations, and have more or less forgotten how to track arrows as written in the rules. I don't know of many folks that do track these things unless they're looking for a specific style. Maybe I would if it [didn't suck](https://deathtrap-games.blogspot.com/2022/02/the-importance-of-encumbrance-and-how.html).

And yes, just about every class has some kind of resource it can manage. But since these are all replenishable resources that return after a good night's sleep, they feel closer to the limited magazine size but unlimited total ammo shown in XCOM above. The Cleric won't lose the ability to heal unless you dump barrels of gold into them over the course of a campaign, unlike the healer classes in Fire Emblem.

As for risks, the (dis)advantage system that everyone can engage with to varying degrees is probably the most common way to modulate risk for an individual roll. The Bless and Bane spells are also about modulating chances of success, both for one's own character and others (possibly the enemies). The Exhaustion system, which is mostly engaged with when pushing through rests, increases risk until it eventually kills the character outright, and must be handled using either a spell or rest. And that's just the parts that everyone is likely to experience. The Wild Magic Sorcerer itself is a subclass wholly dedicated to risk management, while there isn't really a subclass about juggling resources to the same degree.

But didn't I mention in the Fire Emblem section how gold was a factor? Isn't non-combat resources an element of resource tracking? Sort of. Money also came up in the XCOM section, and I was arguing against that being resource management. The fact of money being relevant in XCOM only comes up when you want to purchase new and improved versions of the weapons you're equipping to your soldiers, and you only need enough weapons for each deployed soldier to have one. Even grenades aren't handled individually, you just need to purchase the infinitely resupplying grenade package that gets equipped to some soldiers. And this is effectively what money has been reduced to in D&D: an upgrade path for certain players to take, getting new and better gear.[^money] The only real hits seem to be magic item shops or spell components with an associated gold cost, meaning they can't be replaced with a spellcasting focus.

[^money]: I gave my players a lot of money early on, and they've only recently started to see the end of it. When I say there isn't much intrinsic value in money, I mean that nobody is prepared for having 100 platinum at level 5. There was just a lot of "what do we DO with this?" until it started to run out, multiple magic shops later.

So what about risk management? I think that's a better fit, at least. It seems from both sides of the DM screen that there isn't a great push from the game into conserving resources, and certainly no great worry about running out. I'll get all my spells back after a good night's rest, so the only thought is whether I should use them now or wait for a bigger threat. This reads to me as closer to managing when to reload than what weapon to permanently degrade. The thought isn't whether or not Ulamog the Owlbear will need access to Fireball in a week, but instead whether he'll need it for the next fight. And using it now just means he's in a less optimal state until he can spend the time to get himself back online. Something about this feels more in line with the XCOM approach than the Fire Emblem one.

Conclusion
----------

If D&D has moved from a resource management style of game to a risk management style of game, then we need to account for that when designing encounters and adventures. Laying out a dungeon or even just a single battle means trying to envision what risks should be taken, and how to dangle a reward in a more reasonable manner. The advice in the Dungeon Master's Guide that recommends 6 encounters in an adventuring day seems built for a resource management game, where you're trying to deplete the party's resources so that the last fight can be suitably exciting. If the game is about managing risk, however, then it becomes a little less clear how to make a similar recommendation. Combat Rating also falls apart[^cr] when the goal of a fight isn't to deplete resources, but instead to force risky choices.

[^cr]: Not that it was hanging on by much anyway. The idea of trying to quantify creature strength is a noble one, especially with newer DMs in mind. I just don't think it's one that's been proven effective.

In some sense, I think this leads to encouraging setpiece encounters over random chance, so that the DM has a chance to lay out some risk and set out a reward in a measured way. Unfortunately, setpieces are hard to design through random tables, which seems to be how many D&D-related projects present their design ideas. This in turn means more up-front work in designing encounters, instead of relying on random tables to fill in the gaps.

Is there a better model? Does D&D actually manage some secret third thing that I haven't thought of? Maybe. Probably, even. It doesn't map perfectly onto the ideas we got from XCOM, after all, and we definitely can't expect people who like D&D to like Blades in the Dark or vice versa. But I hope that this exploration is useful, if only to express how D&D isn't the same game it used to be.
