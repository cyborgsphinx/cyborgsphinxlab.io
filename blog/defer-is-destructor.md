---
title: Defer Is A Destructor You Can Forget To Call
date: 2022-11-06
...

Garbage collection is great, isn't it? It lets you stop worrying about memory allocation and instead just write the code that matters. We've solved resource management!

Except, we really didn't. Just look at any other resource that needs to be managed, and you'll see that the management story is not nearly as clean as it is with memory:

```python
def write_to(filename, data):
	f = open(filename, "w")
	f.write(data)

write_to("somefile.txt", "text")
# the file "somefile.txt" is still open, but the handle is not referenced
```

The [python docs](https://docs.python.org/3/tutorial/inputoutput.html#reading-and-writing-files) even have a warning about this:

> Warning
>
> Calling `f.write()` without using the with keyword or calling `f.close()`
> might result in the arguments of `f.write()` not being completely written to
> the disk, even if the program exits successfully.

This is because the data might be cached to be written "later" to batch writes, and the call to `f.close()` is what defines "later".

This is also tricky in the face of exceptions:

```python
def may_throw():
	raise Exception()

def write_to(filename, data):
	f = open(filename, "w")
	f.write(data)
	may_throw()
	f.close()

try:
	write_to("somefile.txt", "text")
except:
	# the file "somefile.txt" is still open, but the handle is not referenced
	print("oops")
```

Of course, the same page shows how python (and java, and C#) address this: the `with` block.

```python
def write_to(filename, data):
	with open(filename, "w") as f:
		f.write(data)

write_to("somefile.txt", "text")
# the file "somefile.txt" has been closed
```

This introduces a new scope, for which `f` is defined. When that scope exits, either by reaching the end or an exception moves execution to an enclosing handler, the file is closed. How? It depends on which language is used, but the idea is typically that the object is responsible for cleaning up the resource.

In C#, this is as simple as implementing the [`IDisposable`](https://learn.microsoft.com/en-us/dotnet/api/system.idisposable?view=net-7.0) interface by defining the `void Dispose()` method.[^1] The object can then be used in a `using` block, and will get cleaned up when that block is exited:

[^1]: Doing research for this post has reminded me of the existence of the `Finalize` method, which is a destructor that gets run by the garbage collector. It seems like this and the `IDisposable` interface conflict perform the same function in different manners, with `IDisposable` being more predictable and `Finalize` being more like a true destructor, just attached to and run by the GC. It also seems like there's some contention about which should be used, which I'd rather avoid. I don't use C# in my daily life. For the purposes of this post, I'm going to focus on `IDisposable`.

```c#
using (MyObject obj = new MyObject())
{
	// use obj
} // obj is cleaned up
// obj is no longer defined
```

In Java, this requires implementing the `AutoCloseable` interface and using the [`try-with-resources`](https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html) construct, which looks like a regular try/catch just with an object defined at the `try`:

```java
try (MyObject obj = new MyObject()) {
	// use obj
} // obj is cleaned up
// obj is no longer defined
```

C# and Java basically just require that you implement the right interfaces and use the right construct in the right place. Python, however, requires [a little bit more](https://docs.python.org/3/library/stdtypes.html#typecontextmanager). Note that this requires two methods, one of which accepts information about an exception *that may not happen*.

> If an exception occurred while executing the body of the with statement, the arguments contain the exception type, value and traceback information. Otherwise, all three arguments are None.

This is more to handle, certainly, and it means there's more that can go wrong (you need to remember to return the object from the `__enter__(self)` method, for example). So Python also has a [context manager decorator](https://docs.python.org/3/library/contextlib.html#contextlib.contextmanager) to make defining a context manager a little easier:

```python
# code copied from https://docs.python.org/3/library/contextlib.html#contextlib.contextmanager
from contextlib import contextmanager

@contextmanager
def managed_resource(*args, **kwds):
    # Code to acquire resource, e.g.:
    resource = acquire_resource(*args, **kwds)
    try:
        yield resource
    finally:
        # Code to release resource, e.g.:
        release_resource(resource)
```

See? Easy. Don't forget the `yield` or the `finally`, or you won't actually call `release_resource(resource)`.

Why?
----

So why is this? Why do we have to jump through so many hoops to manage these resources? Shouldn't a garbage collector help us here, just like it helps us with memory?

The thing is, a garbage collector isn't designed to manage non-memory resources. The garbage collector is just the part of the language runtime responsible for cleaning up after the programmer's memory, essentially wrapping the more classical `malloc` and `free`, or the equivalents for a given platform.

But files aren't handed out using `malloc` and reclaimed using `free`; they use `open` and `close`, which are handled elsewhere in the runtime (often just forwarded to the underlying platform). So for every other resource, we're back at square one, managing these resources ourselves.

Another Approach
----------------

Other languages, like Go, use a different approach: the `defer` statement. `defer` is a language construct that takes a statement (or a code block) that will be run when the scope exits. That's it. It's as easy to use as it gets:

```go
func open(filename string) {
	file := os.Open(filename)
	defer file.Close()
	// use file
} // file is closed
```

This doesn't introduce a new scope, it doesn't require implementing a special method, it just requires using `defer` in the scope you want to check. It even guards against multiple returns, or exceptions if the language has them.

Its major drawback, shared with everything we've discussed so far, is that you can forget to use it. Every single language construct mentioned so far can just be unused when it matters most:

```c#
void Oops()
{
	MyObject obj = new MyObject();
	// oops
} // any resources in MyObject are still in use
```

```java
void oops() {
	MyObject obj = new MyObject();
	// oops
} // any resources in MyObject are still in use
```

```python
def oops():
	obj = MyObject()
	# oops
# any resources in MyObject are still in use
```

```go
func oops() {
	obj := MyObject()
	// oops
} // any resources in MyObject are still in use
```

How To Not Forget
-----------------

There is one way to make sure the user of an object doesn't forget to clean up after themselves: force cleanup to happen automatically. This has many names: destructors, deinitializers, finalizers[^2], RAII[^3]. The idea is simple enough: the object author defines a particular function, and when that object goes out of scope, that function is automatically called. How this is done depends on the language, but the critical part is that it is up to the object's author to define it, not on the users to remember how it should be used. An example:

[^2]: Thanks, C#.

[^3]: Thanks, C++.

```c++
class MyObject
{
	MyObject()
	{
		// acquire resources
	}

	~MyObject()
	{
		// clean up resources
	}
};

void use()
{
	auto obj = MyObject();
	// use obj
} // obj is cleaned up
```

This can even be used to manage memory resources; C++ makes use of the feature in its `unique_ptr<T>`, which ensures that the `<T>` object is deallocated when the pointer goes out of scope.[^4]

[^4]: The folks behind C++ would even like you to [stop using new and delete explicitly](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rr-newdelete).

Obviously it means that there's more burden on the object author to know when to use a destructor, and to write it properly. But that isn't limited to these languages, as we've seen (even Go requires that the `Close()` method on files is written properly in order to have `defer` clean it up). It just consolidates the requirements in one place: at the definition of the object. The user can't forget to run the destructor[^5], because the user isn't going to run the destructor; that's what the runtime (or the compiler, depending on the language) is for.

[^5]: Yes, I know, you can `new MyObject()` in C++ and the destructor will never run. I will refer you back to footnote 4.

Defer Is Destructor
-------------------

But I said that `defer` is a destructor you can forget to call. That means that I think `defer` is no different from a destructor, and we just talked about how destructors are written ahead of time. No ad-hoc nonsense there. Clearly `defer` can do something destructors can't, right?

Well, if that were true, we wouldn't be able to use destructors to make a `defer` equivalent. So let's try to do that. We'll do it in rust, but the ideas should hold for any language with destructors and the ability to hold functions inside variables.

We begin, as is sensible, with the definition of the structure and a public constructor:

```rust
pub struct Defer<F: FnMut()> {
	f: F,
}

impl<F: FnMut()> Defer<F> {
	pub fn new(function: F) -> Self {
		Self {
			f: function,
		}
	}
}
```

This just allows us to keep a function inside an object. The function is one that takes no arguments and returns no value. This models fairly well the idea being captured by `defer`, and is roughly the same as the destructor defined in the C++ object above. Make a note about it being a `FnMut`, we'll come back to that.

Next, we have to define a `Drop` implementation. This is Rust's version of a destructor, and will allow us to run some code when the object leaves scope:

```rust
impl<F: FnMut()> Drop for Defer<F> {
	fn drop(&mut self) {
		(self.f)();
	}
}
```

Easy enough, we've got a `&mut self` which allows us to mutate the contents of the `Defer` object if we want. But inside, we'll just call the held function. This is why we used a `FnMut`, by the way. Because the `Drop` trait requires that we define `drop(&mut self)`, that is a function that takes a mutable reference to `self` and returns nothing, our captured function is the function equivalent: `FnMut`. Technically `FnOnce` would allow us to cover more kinds of functions, but that would require that `Drop` have its method be `drop(self)`. The reason it doesn't is that `drop` gets called *after* the object gets dropped, so having this function take ownership of `self` would cause it to be dropped at the end, and we'd find ourselves in a fun little infinite loop. I could try to wrap the object up in something that could be dropped inside `drop`, but I didn't. Moving on.

This is enough to have the behaviour of `defer`, but it would be nice if we didn't have to use all the ceremony that will arise from using this object as-is. What ceremony? Well...

```rust
use defer::Defer;
let _later = Defer::new(|| {/* code you want to defer */});
```

Technically we can remove the braces (`{` and `}`) if the code is just one statement, but the braces aren't the only thing happening; we need to make it a closure using `||`, and we need to call the `new` function. It'd be nicer if we could wrap that all up in a more usable form.

```rust
#[macro_export]
macro_rules! defer {
	($($s:stmt);*) => {
		$crate::Defer::new(||{ $($s)* })
	};
}
```

This allows us to call the defer in a much nicer way:

```rust
use defer::defer;
let _later = defer!(/* code you want to defer */);
```

Notice how I assigned the result to a variable both times? Well, this is the one downside to the Rust-based defer: if the object isn't assigned to a variable, it gets dropped immediately.

```rust
use defer::defer;
let mut value = 0;
defer!(value += 1);
assert_eq!(value, 0); // this will fail
```

It's possible that we could make the `defer!` macro even closer to the original in Go by generating unique names for variables and doing a `let` bind inside the macro body, but I'm not sure that's necessary to prove the point that we can build our own `defer` using destructors and passing functions as variables.

This is actually a little less powerful than it would be in C++[^6], simply because Rust checks lifetimes and mutability. The `Defer` object mutably borrows anything referenced in the function body you give it, so you may not be able to use it in the rest of your code. And even if we only immutably borrowed it by taking a `Fn` instead of `FnMut`, you still wouldn't be able to mutate it because of the borrow by `Defer`. This isn't a weakness in destructors, it's just a natural consequence of our language of choice. I think that's a reasonable tradeoff to make, since we still have `Drop` and can do the things we'd most commonly want to with a `defer` construct anyway, but it does mean we can't use `Defer` wherever we want like we could in C++ or Go. Just don't use this example code in production. Rust was designed with `Drop` in mind, not `Defer`.

[^6]: To prove my point, the same idea is called [scope guard](https://ricab.github.io/scope_guard/) in the C++ world. I didn't try to replicate it here for two reasons: 1. it is in C++ and I'd rather avoid using C++ in larger examples when possible, and 2. it already exists in a far better form than I'm likely to produce in a weekend.

Improving On The Design
-----------------------

It sure would be nice to be able to have that last example, or something like it, work. We don't need this variable sitting around in our Go code, so it would be nice to not need it in our Rust code. Well, we can at least hide it. Let's write something so that this prints `"before"` and then `"after"`:

```rust
use defer::defer;
defer!(println!("after"));
println!("before");
```

Since we're not providing identifiers for the macro to use, we need to make sure that there's no conflict with the identifiers in the surrounding code. We can use the [gensym crate](https://crates.io/crates/gensym) to achieve that. It's fairly straightforward to use:

```rust
#[macro_export]
macro_rules! defer {
	($($s:stmt);*) => {
		::gensym::gensym!{ $crate::defer!($($s);*) }
	};
	($id:ident, $($s:stmt);*) => {
		let $id = $crate::Defer::new(||{ $($s)* });
	};
}
```

The `gensym!` macro takes a macro invocation and inserts an identifier into that invocation as the first argument. This form matches the second rule in our macro definition, so we run that instead. It should look very familiar, as we just added the `let $id =` and a `;` at the end to appease the almighty compiler.

And that's it! Fairly simple, just one (explicit) dependency, and we have a `defer!` macro that operates roughly like the one in Go. We didn't even have to dig into [procedural macros](https://doc.rust-lang.org/reference/procedural-macros.html). It just, like I said above, wouldn't let you actually use the values passed in later on in your function. Space for improvement, for sure.

Conclusion
----------

I love destructors. They effectively unify managing memory and managing all other resources, by allowing you to manage other resources as though they were memory. This is something I've noticed was missing in managed languages for a while, and was one of the big reasons I was particularly enamoured with Rust when I first heard about it. Solving many different problems using the same underlying tools typically means you can learn something once, and apply it forever. Whereas if you never think about memory, but have to always remember to close files and sockets and clean up everything else, you're still managing resources manually. We can do better than having to manage all resources manually.
