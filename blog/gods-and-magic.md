---
title: The God Problem
date: 2025-01-18
...

The presence of gods offers something of a problem for fantasy worldbuilding, particularly in the context of D&D. In essence, there's a two-sided problem presented by the existence of gods and divine magic. If the gods exist, then you run into every problem Christians have found trying to justify the existence of their own god. In particular, you run into the problem of "if the gods exist, and they love us, then why do bad things happen?" and having to justify whatever answer you give. However, unlike in the real world, D&D reifies divinity with divine magic, accessible to clerics, druids, and paladins. Mechanically the same as arcane magic, there's still a distinction drawn between the categories, and that distinction is in the D&D 5e Player's Handbook.

> Divine magic, as the name suggests, is the power of the gods, flowing from them into the world.[^divine]

[^divine]: D&D 5e Player's Handbook (2014) page 56

So the gods fuel clerics' ability to do magic, meaning they must be real. So if you want to dodge the issues of why bad things happen to good people, you need to do even more work. If there are no gods, and therefore nothing to stop bad things from happening, how do clerics work? If gods do exist, are they just capricious? Do they even know about us? What's the difference between a cleric, who serves a powerful entity and uses that service to fuel magic, and a warlock, who serves a powerful entity and uses that service to fuel magic?

The answers to these and other questions are what worldbuilding is, so how you decide to answer them reflects something about the world you're building. Matt Mercer went with "the gods are real, but had to separate themselves from your world because of an ancient apocalypse" for Exandria, which is a fairly direct answer to most of the questions. Bad things happen because they always happen, and the gods made them worse the last time they tried. Clerics are different from warlocks mostly as a function of the terms of the service - clerics are typically shown having a more personal and caring relationship with their gods, even if those gods are technically just archfey who would otherwise be a warlock patron. This works for Matt, and is players, and is easy enough to emulate.

I didn't start answering this question until a few years into my home game, which has led to a rather different outcome. I had started naming cities after gods from one pantheon, but the cleric in the group was following a god from a different pantheon. I hadn't done the work to nail down which ones exist, so he just went with one he liked from the back of the book. I eventually reconciled this with some syncretism, claiming that everyone had different names for the same entities. Eventually I had the player characters meet a god, which really cements this world as a "gods exist" one. But bad things still need to happen, and the gods can't be a reliable solution to everyone's problems. So they've become something like living ideas, taking whatever shape suits the observer, but unable to act directly on any world other than their own domain.[^domain]

[^domain]: I have had one smite some people, so they clearly can act on their own in some places

This isn't meant as a One True Solution. Like I said before, any worldbuilding question is inherently about the world being built, and maybe reflects something of the builder. I didn't use Matt Mercer's solution because I am not Matt Mercer, and I expect nobody else to use mine because they aren't me.
