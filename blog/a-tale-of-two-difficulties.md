---
title: A Tale of Two Difficulties
date: 2023-07-18
draft: true
...

As a game master for my RPG group, and therefore a budding game designer[^1], I have some thoughts about difficulty in games.
In particular, I think there are multiple kinds of difficulty, and that they are not interchangeable.
I think that this is maybe why difficulty sliders can be odd in video games, where most of the changes amount to larger numbers for the enemies and smaller numbers for the player.
But the multiple kinds of difficulty can also affect what is possible in table top RPGs.

[^1]: Just ask Matt Colville

"But James," I hear you say, "Can't you do anything in table top RPGs through the power of _imagination_?"
Well, sort of.
When I talk about what is possible, I mean what it's reasonable to challenge your players with.
Outside of demanding your players perform equivalent actions IRL in order to determine whether or not they can do something in game, certain kinds of difficulty are inaccessible at the table.
But nobody's asking their players to perform a bench press or perform soliloquies in order to make their characters do things in game, right?
Right?

But before I talk about why I'm saying that some things are reasonable and others are not, I should define some terms.

## Conceptual Difficulty

The first kind of difficulty is what I'm calling Conceptual Difficulty.
It's the measure of how hard it is to reason through to a solution.
Conceptual Difficulty may demand that a player stop actively playing and think about what's happening.

I don't think that's a bad thing to do, but some people may dislike that they had to put down the controller or couldn't just roll a die and move on.
They might even claim that it meant that they weren't playing the game during that time.

I disagree, however.
I would say that you're still playing the game, because you're having to imagine the fictional world as though it were real.
You can't just muscle through to a solution, you have to treat the world as though it were real and consistent.
I like that.

It should be said, however, that this relies on the fictional world being consistent.
If the solution to a problem can't be reasoned out and instead requires sheer dumb luck, that's a different kind of difficulty entirely.

This kind of difficulty is easy to imagine slotting into an RPG.
We do it all the time, in fact.
Puzzles are inherently conceptual in their difficulty.
They can't typically be solved by fumbling through with good rolls, but instead by engaging with the fictional world on its terms.

## Procedural Difficulty

The other kind of difficulty is what I'm calling Procedural Difficulty, or Execution Difficulty.
It's the measure of how difficult it is to do the thing.
To execute on the solution once the solution is known.
It's in things like timing of attacks, or moving out of a blast zone in a limited time.
Dark Souls is famous for its execution difficulty, and arguably popularized the idea among digital gamers.
Speedrunning also typically becomes about difficulty of execution, even for games that at first blush seem more tuned for some other kind of difficulty.

This is what many people seem to mean when they call a video game difficult, and it's definitely being hit by the bigger number difficulty slider mentioned above.
If your enemies hurt you more and you hurt them less, executing on the same solution got decidedly harder.

This is the difficulty that can't easily be slotted into table top RPGs.
The obvious answer is to raise the difficulty of checks and such, but since checks are typically done in a random manner, all that's really going on is making it harder to randomly succeed.
Without leaning significantly on narration, this can be something of a let down.

And, yes, there are likely other categories of difficulty beyond these two.
They're just the ones I can easily reach for in my own mind.
It's my blog, you get my biases.

## Next Steps

So we've identified a problem.
An issue, even.
There are certain kinds of difficulty that are unreasonable to fit into a game of D&D, for one reason or another.
Maybe challenging your players' ability to roll dice isn't what you're looking for.
What do we do with this?

That's where I'm not sure.
We can't just replace all Procedural Difficulty with Conceptual Difficulty, since they express different kinds of challenge.
Not everyone wants to turn D&D into a puzzle game where they have to stop talking and think every 20 minutes.

The different kinds of difficulty are really about encouraging different kinds of engaging with the fictional world.
Conceptual Difficulty asks that players take the world's laws and history seriously.
It asks that they believe in this world, and poke and prod, until a new discovery falls out.
Meanwhile Procedural Difficulty asks that they strive for better, for mastery even.
Maybe system mastery replaces procedural difficulty, and thinking of sick combos is its own reward.
I'm not sure.
