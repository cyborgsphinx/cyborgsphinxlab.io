---
title: Hello Worlds Are Not Created Equal
date: 2024-03-30
...

Hello World is probably one of the most famous computer programs.
It's even got [a Wikipedia page](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program).
But it seems like there's far more emphasis placed on the effect of the program itself than on the effect of the source code on the reader.
And I consider that a problem.
A failing, even, of the software development community.

Let's take a look at what I'm going to call the canonical example of Hello World in C.
This is Hello World as I understand it, written such that it produces no warnings or errors.

```c
#include <stdio.h>
int main() {
	printf("hello, world");
}
```

In 4 lines, you get a program that prints text to the terminal.
If you work in the terminal, you can immediately see the results of what you typed in.
Or copied in.
Point is, you put text into a file, ran a compiler, and ran the output, and saw something happen.
Brilliant!

On top of that, there are a bunch of places for further questions.
What's that `#include` bit doing?
Why is `stdio.h` enclosed in `<>`?
What does `int main()` mean?
Why, when I ran this program, did the output not get its own line?
The terminal, after all, is likely to look like this:

```
$ ./a.out
hello world$
```

What I'm getting at is that this program is **instructive**.
It prompts further questions from the student, which can encourage further learning.

But this breaks down with what I often see as the canonical Python version:

```python
print("hello, world")
```

This doesn't look very impressive.
I don't really have any questions.
But I also don't know where to go from here.
I just have a single line, which does a thing.
If I'm trying to learn programming for the first time, I might not even know there are other questions I might *want* to ask.

Even Haskell, noted [supposedly complex language](https://alvinalexander.com/sites/default/files/2017-06/how-write-hello-world-in-haskell_0.jpg), is easy enough to write:

```haskell
main = printStrLn "hello, world"
```

Not a monad in sight.
The syntax is a little goofy if you're not expecting it, and that can cause some questions, but nothing that really gets at what it is to write Haskell.
A step up from Python above, to be sure, but this isn't really representative of what an actual program would look like.

Java, which has a lot to not like, at least also prompts similar questions:

```java
import java.lang.System;
public class HelloWorld {
	public static void main(String[] args) {
		System.out.println("hello, world");
	}
}
```

There's a lot going on here, all of it worth asking about, some of it even worth answering right away.
Why is `main` wrapped in a `class`?
What does `public` mean?
What does `static` mean?
Why does `String` have `[]` after it?

So how do we get Python something more instructive?
Easy:

```python
def hello():
	print("hello, world")
if __name__ == "__main__":
	hello()
```

There.
Now the python student has a lot of questions.
And only a handful are useful early on, so I'm not even sure I'd actually recommend this.

Interestingly, Rust has two very different kinds of Hello Worlds, depending on how much (and what) you want to introduce.
What I would consider canonical:

```rust
fn main() {
	println!("hello, world");
}
```

And if you want to forego macros:

```rust
use std::io::{self, Write};
fn main() {
	let _ = io::stdin().lock().write(b"hello, world");
}
```

On second thought, just use the macro version.
