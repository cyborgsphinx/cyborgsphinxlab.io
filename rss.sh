#! /bin/sh

SITE_PREFIX="https://cyborgsphinx.com"

makeitems() {
    for entry in $@
    do
        date=$(grep 'date:' $entry | sed 's/date: //')
        IFS='-' read -ra pieces <<< "$date"
        year="${pieces[0]}"
        month_idx="${pieces[1]}"
        months=("pad" "Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
        month="${months[$month_idx]}"
        day="${pieces[2]}"
        title=$(grep 'title:' $entry | sed 's/title: //' | sed 's/| CS//')
        link=$(echo $entry | sed 's/md/html/')
        description=$(awk -v RS= 'BEGIN{p=3}/^[A-Z]/{print} {if (--p == 0) exit}' $entry | sed 's/\[\^[0-9+]\]//')
        echo "<item>"
        echo "<title>$title</title>"
        echo "<link>$SITE_PREFIX/$link</link>"
        echo "<guid>$SITE_PREFIX/$link</guid>"
        echo "<description><![CDATA[$description]]></description>"
        echo "<pubDate>$day $month $year 12:00 PST</pubDate>"
        echo "</item>"
    done
}

echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'
echo "<channel>"
echo "<title>Cyborgsphinx Blog</title>"
echo "<link>$SITE_PREFIX</link>"
echo -n '<atom:link href="'
echo -n $SITE_PREFIX/rss.xml
echo '" rel="self" type="application/rss+xml" />'
echo "<description>Some guy's blog</description>"

makeitems $(fd -e md . blog -x grep -L "draft: *true")

echo "</channel>"
echo "</rss>"